# Less1.8

## Описание задания 

Иногда нужно контейнерам нужен общий волум в режиме рид онли.
Что бы закрепить тему с волумами сделаем задачу.
Берем композ с прошлого задания (2 нжинкса + графана)
И модифицируем его таким образом ,что бы не копировать конфиги каждому контейнеру ,а создали в композе волюм и подключили бы его к 2м нжинксам.

[Прошлое задание](https://gitlab.com/czkjz/less1.6 "Прошлое задание")

## Изменения в сравнении с прошлым заданием
Добавлен `.env`

Изменен конфиг `nginx`

Изменена секция `nginx1` в docker-compose.yml:

```yaml
nginx1:
    restart: always
    image: nginx:alpine
    container_name: nginx1
    ports:
      - "8001:8000"
    volumes:
      - nginx_vol:${WEB_DESTINATION_PATH}:ro
    depends_on:
      - grafana
    networks:
      - salam1
```

Изменена секция `nginx2` в docker-compose.yml:

```yaml
  nginx2:
    restart: always
    image: nginx:alpine
    container_name: nginx2
    ports:
      - "8002:8000"
    volumes:
      - nginx_vol:${WEB_DESTINATION_PATH}:ro
    depends_on:
      - grafana
    networks:
      - salam2
```

Изменена секция `volumes` в docker-compose.yml:

```yaml
volumes:
  grafana_data:
  nginx_vol:
    name: nginx_vol
    driver: local
    driver_opts:
      type: none
      device: $PWD/${WEB_APP_PATCH}
      o: bind
```


